package com.luv2code.springdemo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.springdemo.dao.CustomerDAO;
import com.luv2code.springdemo.entity.Customer;
import com.luv2code.springdemo.utils.MathCustomfunctions;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private MathCustomfunctions mathCustfunc;

	
	@Override
	@Transactional
	public List<Customer> getCustomers() {
		return customerDAO.getCustomers();
	}

	@Override
	@Transactional
	public void saveCustomer(Customer theCustomer) {
	
		customerDAO.saveCustomer(theCustomer);	
	}

	@Override
	@Transactional
	public Customer getCustomer(int theId) {

		return customerDAO.getCustomer(theId);
	}

	@Override
	@Transactional
	public void deleteCustomer(int theId) {
		
		customerDAO.deleteCustomer(theId);
	}

	public int nextNumInSeries(int a, int b, int c, int d, int e, int f) {

		int []  arrayNums = {a,b,c,d,e,f};
		if (!mathCustfunc.APseriesCheck(arrayNums).equalsIgnoreCase("SKIP")) //cond1
			return(Integer.valueOf(mathCustfunc.computeNextterm()));
		else
			if (!mathCustfunc.GPseriesCheck(arrayNums).equalsIgnoreCase("SKIP"))//cond2
				return(Integer.valueOf(mathCustfunc.computeNextterm()));
			else
				if (!mathCustfunc.ProgressiveGPseriesCheck(arrayNums).equalsIgnoreCase("SKIP"))
					return(Integer.valueOf(mathCustfunc.computeNextterm()));
				else
					if (!mathCustfunc.FIBseriesCheck(arrayNums).equalsIgnoreCase("SKIP"))
						return(Integer.valueOf(mathCustfunc.computeNextterm()));
					else
						if (!mathCustfunc.PRIMEseriesCheck(arrayNums).equalsIgnoreCase("SKIP"))//cond5
							return(Integer.valueOf(mathCustfunc.computeNextterm()));
						else
							if (!mathCustfunc.PowerseriesCheck(arrayNums).equalsIgnoreCase("SKIP"))
								return(Integer.valueOf(mathCustfunc.computeNextterm()));
							else
								return 0;
	}
}
